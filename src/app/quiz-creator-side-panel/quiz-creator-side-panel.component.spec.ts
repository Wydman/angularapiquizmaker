import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizCreatorSidePanelComponent } from './quiz-creator-side-panel.component';

describe('QuizCreatorSidePanelComponent', () => {
  let component: QuizCreatorSidePanelComponent;
  let fixture: ComponentFixture<QuizCreatorSidePanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizCreatorSidePanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizCreatorSidePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
