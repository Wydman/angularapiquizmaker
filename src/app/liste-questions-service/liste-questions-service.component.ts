import {Component, OnInit} from '@angular/core';
import {QuestionService} from '../service/question.service';
import {Question} from '../Models/question';

@Component({
  selector: 'app-liste-questions-service',
  templateUrl: './liste-questions-service.component.html',
  styleUrls: ['./liste-questions-service.component.css']
})
export class ListeQuestionsServiceComponent implements OnInit {

  questionList: Question[] = [];

  constructor(private quizMakerService: QuestionService) {
    this.GetAllQuestions();
  }


  GetAllQuestions() {
    this.quizMakerService.getQuestionList().subscribe(peoples => {
      this.questionList = peoples;
      this.questionList.reverse();
    },error => {
      alert('Error')
      }
    );

  }

  ngOnInit(): void {
  }
}
