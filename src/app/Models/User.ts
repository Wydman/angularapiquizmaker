import {Input} from '@angular/core';

export class User {

  userID: number;
  userName: string;
  userPassword: string;
  userBestScore:number;

  constructor(userName: string, userPassword: string ='') {
    this.userName = userName;
    this.userPassword = userPassword;
  }
}
