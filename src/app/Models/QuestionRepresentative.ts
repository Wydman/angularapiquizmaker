export class QuestionRepresentative {
  id: boolean;
  answer: string;
  answerResult: boolean;
  movieName: string;

  constructor(answer: string = '', movieName: string ='') {
    this.answer = answer;
    this.movieName = movieName;
  }
}
