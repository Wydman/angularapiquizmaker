export class Question {
  id: number;
  displayableText: string;
  questionText: string;
  questionAnswer: string;
  movieName: string;

  constructor(id:number, displayableText:string, questionText:string, questionAnswer:string,movieName:string) {
    this.id = id;
    this.displayableText = displayableText;
    this.questionText = questionText;
    this.questionAnswer = questionAnswer;
    this.movieName = movieName;
  }
}
