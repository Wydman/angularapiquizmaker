import {Component, Input, OnInit} from '@angular/core';
import {QuestionService} from '../service/question.service';
import {User} from '../Models/User';
import {UserService} from '../service/user-service';

@Component({
  selector: 'app-login-panel',
  templateUrl: './login-panel.component.html',
  styleUrls: ['./login-panel.component.css']
})
export class LoginPanelComponent implements OnInit {

  @Input() inputLogin: String;
  @Input() inputPassword: String;
  @Input() displayLoginMessage: boolean = false;
  @Input() userDoesntExist: boolean = null;
  @Input() createAccount: boolean = null;
  @Input() loginMessage: string = null;

  @Input() signInName: string;
  @Input() signInPassword: string;

  @Input() scoreDisplay:string;

  constructor(private userService: UserService) {
  }


  tryConnect(login: string, password: string) {
    this.displayLoginMessage = true;

    this.userService.searchUserName(login).subscribe(isRegistered => {

      if (isRegistered) {
        this.userService.tryConnect(login, password).subscribe(passwordMatch => {
          if (passwordMatch) {
            this.loginMessage = 'vous êtes connecté';

            this.userService.GetUserByName(login).subscribe(user => {
              this.userService.currentUser = user;
              this.userService.userConnected = true;
            });

          } else {
            this.loginMessage = 'mauvais mot de passe NOOB !';
          }
        });
      } else {
        this.loginMessage = 'Cet utilisateur n\'existe pô !';
        this.userDoesntExist = true;
      }
    });
  }


  createNewUser(userName: string, userPassword: string) {
    this.userService.createNewUser(userName, userPassword).subscribe(
      user => {
        console.log(user);
        if (user.userID == 0) {
          this.displayLoginMessage = true;
          this.loginMessage = 'Ce pseudo est déja atribué';
        }
        else{
          this.createAccount = false;
          this.tryConnect(userName,userPassword);
        }
      }
    );
  }

  disconnect() {
    this.userService.currentUser = null;
    this.userService.userConnected = false;
  }

  ngOnInit(): void {
    this.userService.shareScoreEvent.subscribe(scoreToAdd =>{
      this.userService.currentUser.userBestScore+=scoreToAdd;
      console.log('Votre score est de : '+ this.userService.currentUser.userBestScore);
    });
  }

}
