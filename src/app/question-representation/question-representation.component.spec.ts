import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionRepresentationComponent } from './question-representation.component';

describe('QuestionRepresentationComponent', () => {
  let component: QuestionRepresentationComponent;
  let fixture: ComponentFixture<QuestionRepresentationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionRepresentationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionRepresentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
