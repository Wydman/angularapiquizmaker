import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Question} from '../Models/question';
import {QuestionService} from '../service/question.service';
import {Observable} from 'rxjs';
import {UserService} from '../service/user-service';

@Component({
  selector: 'app-question-representation',
  templateUrl: './question-representation.component.html',
  styleUrls: ['./question-representation.component.css']
})
export class QuestionRepresentationComponent implements OnInit {
  @Input() question: Question;
  @Input() questionText: string;
  @Input() userAnswer: string;
  @Input() movieName: string;
  @Input() buttonState: boolean;

  isUserRight: boolean = false;
  isUserWrong: boolean = false;

  showQuestion = true;
  @Input() showAnswerInput = false;

  onChange(str: string) {

  }

  constructor(
    private questionService: QuestionService, private  userService: UserService
  ) {
  }


  TryAnswer(id: number, str: string) {
    this.questionService.postTryAnswer(id, str).subscribe(answer => {
      this.isUserRight = answer.answerResult;
      if (answer.answerResult) {
        this.isUserRight = true;
        this.buttonState = true;
        this.userService.addScore(8).subscribe(user =>{
          this.userService.currentUser = user;
          console.log(user);
        });
      } else {
        this.isUserWrong = true;
        this.buttonState = true;
      }
    });

    setTimeout(() => this.showQuestion = false, 2000);

  }


  ngOnInit() {
  }

}
