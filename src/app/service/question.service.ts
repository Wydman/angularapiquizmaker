import {Component, EventEmitter, Injectable, OnInit, Output} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Question} from '../Models/question';
import {map} from 'rxjs/operators';
import {stringify} from 'querystring';
import {QuestionRepresentative} from '../Models/QuestionRepresentative';
import {User} from '../Models/User';


@Injectable()
export class QuestionService{

  constructor(private http: HttpClient) {

  }

  getQuestionList(): Observable<Question[]> {
    return this.http.get<Question[]>('http://localhost:8080/questions');
  }

  getNewQuestion(movieName:string): Observable<Question> {
    return this.http.post<Question>('http://localhost:8080/questions', new QuestionRepresentative('',movieName));
  }

  postTryAnswer(id:number,answer:string): Observable<QuestionRepresentative> {
    return this.http.post<QuestionRepresentative>(`http://localhost:8080/questions/${id}/tryAnswer`,new QuestionRepresentative(answer));
  }
}
