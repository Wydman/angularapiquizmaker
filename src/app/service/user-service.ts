import {EventEmitter, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../Models/User';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {isNumber, isString} from 'util';

@Injectable()
export class UserService{

  currentUser: User;
  userConnected:boolean = false;
  shareScoreEvent = new EventEmitter<number>();

  constructor(private http:HttpClient) {
  }
  GetUserByName(userName:string): Observable<User> {
    return this.http.post<User>('http://localhost:8080/users/getUserByName', new User(userName));
  }

  searchUserName(userName:string): Observable<boolean> {
    return this.http.post<boolean>('http://localhost:8080/users/searchUser', new User(userName));
  }

  createNewUser(userName:string,userPassword:string): Observable<User> {
    return this.http.post<User>('http://localhost:8080/users/create', new User(userName,userPassword));
  }

  tryConnect(userName:string,userPassword:string): Observable<boolean> {
    return this.http.post<boolean>(`http://localhost:8080/users/tryConnect`,new User(userName,userPassword));
  }

  addScore(scoreToAdd:number){
    let userID = this.currentUser.userID;
    return this.http.post<User>(`http://localhost:8080/users/addPoints`,{userID:userID, pointsToAdd:scoreToAdd});
  }
}
