import {Component, Input, OnInit} from '@angular/core';
import {Question} from '../Models/question';
import {QuestionService} from '../service/question.service';

@Component({
  selector: 'app-user-generate-random-question-panel',
  templateUrl: './user-generate-random-question-panel.component.html',
  styleUrls: ['./user-generate-random-question-panel.component.css']
})
export class UserGenerateRandomQuestionPanelComponent implements OnInit {

  @Input() userMovieRequest:string;
  @Input() newQuestionList:Question[] = [];

  onChange(str:string){

  }

  constructor(private questionMakerServiceComponent:QuestionService) { }

  CreateQuestion(str:string){
   this.questionMakerServiceComponent.getNewQuestion(str).subscribe(
     question =>{
      this.newQuestionList.push(question);
      this.newQuestionList.reverse()
       this.userMovieRequest = '';
      console.log(question)


     }
   );
  }

  ngOnInit() {
  }

}
