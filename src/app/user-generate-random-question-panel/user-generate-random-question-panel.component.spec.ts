import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserGenerateRandomQuestionPanelComponent } from './user-generate-random-question-panel.component';

describe('UserGenerateRandomQuestionPanelComponent', () => {
  let component: UserGenerateRandomQuestionPanelComponent;
  let fixture: ComponentFixture<UserGenerateRandomQuestionPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserGenerateRandomQuestionPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserGenerateRandomQuestionPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
