import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { QuestionRepresentationComponent } from './question-representation/question-representation.component';
import { ListeQuestionsServiceComponent } from './liste-questions-service/liste-questions-service.component';
import { UserSidePanelComponent } from './user-side-panel/user-side-panel.component';
import { RouterModule, Routes } from '@angular/router';
import { QuizCreatorSidePanelComponent } from './quiz-creator-side-panel/quiz-creator-side-panel.component';
import {HttpClientModule} from '@angular/common/http';
import { UserGenerateRandomQuestionPanelComponent } from './user-generate-random-question-panel/user-generate-random-question-panel.component';
import { UserAnswerQuizPanelComponent } from './user-answer-quiz-panel/user-answer-quiz-panel.component';
import { QuestionService } from './service/question.service';
import { Question } from './Models/question';
import {FormsModule} from '@angular/forms';
import { LoginPanelComponent } from './login-panel/login-panel.component';
import {UserService} from './service/user-service';


const  appRoutes:Routes =[
  { path: 'creator', component:QuizCreatorSidePanelComponent },
  { path: 'user', component:UserSidePanelComponent },
  { path: 'user/generate', component:UserGenerateRandomQuestionPanelComponent },
  { path: 'user/answer', component:UserAnswerQuizPanelComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    QuestionRepresentationComponent,
    ListeQuestionsServiceComponent,
    UserSidePanelComponent,
    QuizCreatorSidePanelComponent,
    UserGenerateRandomQuestionPanelComponent,
    UserAnswerQuizPanelComponent,
    LoginPanelComponent,
  ],
  imports: [
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    FormsModule
  ],
  providers: [
    QuestionService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
