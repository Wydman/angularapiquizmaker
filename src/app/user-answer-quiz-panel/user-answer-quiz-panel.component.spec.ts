import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAnswerQuizPanelComponent } from './user-answer-quiz-panel.component';

describe('UserAnswerQuizPanelComponent', () => {
  let component: UserAnswerQuizPanelComponent;
  let fixture: ComponentFixture<UserAnswerQuizPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAnswerQuizPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAnswerQuizPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
